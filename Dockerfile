FROM ruby:2.5.0
RUN mkdir /app
WORKDIR /app
RUN gem install bundler
COPY . /app
RUN bundle install
